package com.loan.management.dao.impl;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.loan.management.config.LoanManagementRepostory;
import com.loan.management.dao.LoanMangementDao;
import com.loan.management.dto.LoanManagement;
 
@Repository
public class LoanMangementDaoImpl implements LoanMangementDao{
	@Autowired
	private LoanManagementRepostory loanMangementRepostory;
	@Override
	public void saveApplication(LoanManagement loanManagement) {
		  
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");
		Date date = new Date();		 
		loanManagement.setTimeCreated(dateFormat.format(date));
		loanMangementRepostory.save(loanManagement);
		
	}
	@Override
	public Iterable<LoanManagement> loadApplication(String creditStatus) {
		 
		return loanMangementRepostory.findByCreditStatus(creditStatus);	 
		
		 
	}
	@Override
	public void updateApplication(LoanManagement loanManagement) {
		if(loanMangementRepostory.exists(loanManagement.getApplicationId())){
	 	 
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY HH:mm:ss");
		Date date = new Date();		 
		loanMangementRepostory.updateCreditStatus(loanManagement.getApplicationId(), loanManagement.getCreditStatus(), dateFormat.format(date)) ;
		}else{
			
		}
	}

}
