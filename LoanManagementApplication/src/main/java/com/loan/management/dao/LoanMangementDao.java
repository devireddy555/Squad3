package com.loan.management.dao;

import com.loan.management.dto.LoanManagement;

public interface LoanMangementDao {
	
	public void saveApplication(LoanManagement loanManagement);
	public Iterable<LoanManagement> loadApplication(String creditStatus);
	public void updateApplication(LoanManagement loanManagement);
	 
}
