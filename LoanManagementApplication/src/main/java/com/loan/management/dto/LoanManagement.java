package com.loan.management.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="loanmanagement")
public class LoanManagement {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "application_id")
	private int applicationId;
	
	@Column(name = "first_name")
	private String firstName;
	@Column(name = "last_name")
	private String lastName;
	@Column(name = "address")
	private String address;
	@Column(name = "credit_status")
	private String creditStatus;	 
	@Column(name = "application_income")
	private double applicationIncome;
	@Column(name = "loan_amount")
	private double loanAmount;	 
	@Column(name = "time_created")
	private String timeCreated;
 
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCreditStatus() {
		return creditStatus;
	}
	public void setCreditStatus(String creditStatus) {
		this.creditStatus = creditStatus;
	}
	public double getApplicationIncome() {
		return applicationIncome;
	}
	public void setApplicationIncome(double applicationIncome) {
		this.applicationIncome = applicationIncome;
	}
	public double getLoanAmount() {
		return loanAmount;
	}
	public void setLoanAmount(double loanAmount) {
		this.loanAmount = loanAmount;
	}
	public String getTimeCreated() {
		return timeCreated;
	}
	 
	public void setTimeCreated(String timeCreated) {
		this.timeCreated = timeCreated;
	}
	public int getApplicationId() {
		return applicationId;
	}
	@Override
	public String toString() {
		return "LoanManagement [applicationId=" + applicationId + ", firstName=" + firstName + ", lastName=" + lastName
				+ ", address=" + address + ", creditStatus=" + creditStatus + ", applicationIncome=" + applicationIncome
				+ ", loanAmount=" + loanAmount + ", timeCreated=" + timeCreated + "]";
	}
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}
	 
	
}
