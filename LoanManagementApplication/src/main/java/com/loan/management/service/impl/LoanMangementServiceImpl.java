package com.loan.management.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.loan.management.dao.LoanMangementDao;
import com.loan.management.dto.LoanManagement;
import com.loan.management.service.LoanMangementService;
@Service
public class LoanMangementServiceImpl implements LoanMangementService {
	@Autowired
	private LoanMangementDao loanMangementDao;
	@Override
	public void saveApplication(LoanManagement loanManagement) {
		loanMangementDao.saveApplication(loanManagement);
		
	}
	@Override
	public Iterable<LoanManagement> loadApplication(String creditStatus) {
		// TODO Auto-generated method stub
		return loanMangementDao.loadApplication(creditStatus);
	}
	@Override
	public void updateApplication(LoanManagement loanManagement) {
		// TODO Auto-generated method stub
		  loanMangementDao.updateApplication(loanManagement);
	}

}
