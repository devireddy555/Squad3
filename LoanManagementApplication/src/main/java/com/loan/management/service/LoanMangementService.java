package com.loan.management.service;

import com.loan.management.dto.LoanManagement;

public interface LoanMangementService {
	public void saveApplication(LoanManagement loanManagement);
	public Iterable<LoanManagement> loadApplication(String creditStatus);
	public void updateApplication(LoanManagement loanManagement);
	 
}
