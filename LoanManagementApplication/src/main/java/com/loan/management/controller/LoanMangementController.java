package com.loan.management.controller;

import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.loan.management.dto.LoanManagement;
import com.loan.management.service.LoanMangementService;
 
@RestController 
public class LoanMangementController {
	@Autowired
	private LoanMangementService loanMangementService;
	
	@RequestMapping(value = "/createapplication", method = RequestMethod.POST,consumes=MediaType.APPLICATION_JSON)	
	public void saveApplication(@RequestBody LoanManagement loanManagement){
		loanMangementService.saveApplication(loanManagement);
	}
	@RequestMapping(value = "/loadapplication", method = RequestMethod.GET,produces=MediaType.APPLICATION_JSON)
	public Iterable<LoanManagement> loadApplication(@QueryParam("creditStatus") String creditStatus){
		
		return loanMangementService.loadApplication(creditStatus);
	}
	
	@RequestMapping(value = "/updateapplication", method = RequestMethod.PUT,consumes=MediaType.APPLICATION_JSON)	
	public void updateApplication(@RequestBody LoanManagement loanManagement){
		loanMangementService.updateApplication(loanManagement);
	}
}
