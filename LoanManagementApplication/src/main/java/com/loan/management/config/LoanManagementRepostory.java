package com.loan.management.config; 

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional; 
import com.loan.management.dto.LoanManagement; 
 
@Transactional
public interface  LoanManagementRepostory  extends CrudRepository<LoanManagement, Integer>{
	   
     Iterable<LoanManagement> findByCreditStatus( String creditStatus);
     @Modifying
     @Query("UPDATE LoanManagement loanManagement SET loanManagement.creditStatus = :creditStatus,loanManagement.timeCreated = :timeCreated WHERE loanManagement.applicationId = :applicationId")
     int updateCreditStatus(@Param("applicationId") int applicationId, @Param("creditStatus") String creditStatus,@Param("timeCreated") String timeCreated);
     
}
